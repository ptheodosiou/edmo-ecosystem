jQuery(document).ready(async function ($) {
  const waitFor = (ms = 500) => new Promise((r) => setTimeout(r, ms));

  const waitForElement = (selector) => {
    return new Promise((resolve) => {
      if (document.querySelectorAll(selector)) {
        return resolve(document.querySelectorAll(selector));
      }

      const observer = new MutationObserver((mutations) => {
        if (document.querySelectorAll(selector)) {
          resolve(document.querySelectorAll(selector));
          observer.disconnect();
        }
      });

      observer.observe(document.body, {
        childList: true,
        subtree: true,
      });
    });
  };

  waitForElement("a[href='https://twitter.com/imec_smit']")
    .then((el) => {
      const schoolEl = $(el).clone();
      const parent = $(el).parent();
      schoolEl.attr("href", "https://twitter.com/Brussels_School");
      schoolEl.attr("title", "Brussels School");
      schoolEl.appendTo(parent);
    })
    .catch((error) => {
      console.log("Element not exists");
      console.log(error.toString());
      console.log(error);
    });

  waitForElement("a.socials-item:last-of-type")
    .then((elements) => {
      elements.forEach((el) => {
        const _element = $(el).clone();
        const parent = $(el).parent();
        _element.attr("href", "mailto:belux@edmo.eu");
        _element.attr("title", "Contact");
        _element.appendTo(parent);
      });
    })
    .catch((error) => {
      console.log("Element not exists");
      console.log(error.toString());
      console.log(error);
    });
});
