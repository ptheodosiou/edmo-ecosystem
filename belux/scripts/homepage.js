jQuery(document).ready(async function ($) {
  const waitFor = (ms = 500) => new Promise((r) => setTimeout(r, ms));

  const isDutch = () => {
    const url = window.location;
    return !!String(url).match(/\/nl\//);
  };

  const isFrench = () => {
    const url = window.location;
    return !!String(url).match(/\/fr\//);
  };

  const isGerman = () => {
    const url = window.location;
    return !!String(url).match(/\/de\//);
  };

  const isLuxembourgish = () => {
    const url = window.location;
    return !!String(url).match(/\/lb\//);
  };

  const waitForElements = (selector) => {
    return new Promise((resolve) => {
      if (document.querySelectorAll(selector)) {
        return resolve(document.querySelectorAll(selector));
      }

      const observer = new MutationObserver((mutations) => {
        if (document.querySelectorAll(selector)) {
          resolve(document.querySelectorAll(selector));
          observer.disconnect();
        }
      });

      observer.observe(document.body, {
        childList: true,
        subtree: true,
      });
    });
  };

  waitForElements(".gem-featured-post-meta-categories > span")
    .then((elements) => {
      elements.forEach((el) => {
        $(el).clone();
        let text = "";
        if (isDutch()) text = "Keuze van de dag";
        else if (isFrench()) text = "Choix de la rédaction";
        else if (isGerman()) text = "Wahl der Redaktion";
        else if (isLuxembourgish()) text = "De Choix vun der Redaktioun";
        else text = "Editor's choice";
        $(el).text(text);
      });
    })
    .catch((error) => {
      console.log("Element not exists");
      console.log(error.toString());
      console.log(error);
    });

  await waitFor(1000);

  waitForElements("ul.vc_grid-filter")
    .then((elements) => {
      let list = $("ul.vc_grid-filter");
      let items = list.children();
      let firstItem = items.first();

      let elementIndex = [...items].findIndex((x) => {
        const text = $(x).text().toLowerCase();
        return text === "ukraine" || text === "oekraïne";
      });

      if (elementIndex < 0) return;

      items.eq(0).after(items[elementIndex]);
    })
    .catch((error) => {
      console.log("Element not exists");
      console.log(error.toString());
      console.log(error);
    });

  waitForElements(".blog.blog-style-compact-2 > article.post").then((elements) => {
    elements.forEach((el) => {
      
      const title = $(el).find(".gem-news-item-title a").text();
      $(el).attr("title", title)
    });
  })
});
