jQuery(document).ready(async function ($) {
  const waitFor = (ms = 500) => new Promise((r) => setTimeout(r, ms));

  const waitForElement = (selector) => {
    return new Promise((resolve) => {
      if (document.querySelector(selector)) {
        return resolve(document.querySelector(selector));
      }

      const observer = new MutationObserver((mutations) => {
        if (document.querySelector(selector)) {
          resolve(document.querySelector(selector));
          observer.disconnect();
        }
      });

      observer.observe(document.body, {
        childList: true,
        subtree: true,
      });
    });
  };

  waitForElement(".breadcrumbs-container .breadcrumbs")
    .then((el) => {
      $(el).text("Editor's Choice");
    })
    .catch((error) => {
      console.log("Element not exists");
      console.log(error.toString());
      console.log(error);
    });
});
