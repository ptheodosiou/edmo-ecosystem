jQuery(document).ready(async function ($) {
  const waitFor = (ms = 500) => new Promise((r) => setTimeout(r, ms));

  const waitForElement = (selector) => {
    return new Promise((resolve) => {
      if (document.querySelectorAll(selector)) {
        return resolve(document.querySelectorAll(selector));
      }

      const observer = new MutationObserver((mutations) => {
        if (document.querySelectorAll(selector)) {
          resolve(document.querySelectorAll(selector));
          observer.disconnect();
        }
      });

      observer.observe(document.body, {
        childList: true,
        subtree: true,
      });
    });
  };

  try {
    const copyrightBoxEl = await waitForElement(
      ".fact-check-metabox .fact-check-metabox__top .copyright"
    );
    const pictureEl = await waitForElement(".post-featured-content picture");

    if (!copyrightBoxEl) return;

    $(copyrightBoxEl).detach().appendTo($(pictureEl));
  } catch (error) {
    console.log("Something went wrong");
    console.log(error.toString());
    console.log(error);
  }
});
