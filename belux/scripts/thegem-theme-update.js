jQuery(document).ready(async function ($) {
  const waitFor = (ms = 500) => new Promise((r) => setTimeout(r, ms));

  const waitForElement = (selector) => {
    return new Promise((resolve) => {
      if (document.querySelectorAll(selector)) {
        return resolve(document.querySelectorAll(selector));
      }

      const observer = new MutationObserver((mutations) => {
        if (document.querySelectorAll(selector)) {
          resolve(document.querySelectorAll(selector));
          observer.disconnect();
        }
      });

      observer.observe(document.body, {
        childList: true,
        subtree: true,
      });
    });
  };

  waitForElement(".thegem-update-notice-new.notice.notice-error")
    .then((elements) => {
      let _elements = [...elements];

      _elements.forEach((el) => {
        $(el).remove();
      });
    })
    .catch((error) => {
      console.log("Element not exists");
      console.log(error.toString());
      console.log(error);
    });
});
