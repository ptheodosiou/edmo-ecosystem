jQuery(document).ready(async function ($) {
  const waitFor = (ms = 500) => new Promise((r) => setTimeout(r, ms));

  function buildListItem(value, index) {
    // return `<li data-original-index="${index}"><a tabindex="${index}" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">${value}</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>`;
    return `<li data-original-index="${index}"><a tabindex="${index}" class="" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">${value}</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>`;
  }

  async function buildSelectBox() {
    await waitFor();

    // Authors
    let authorItems = $(".column-post_meta_authors select option");
    // let _authorItems = [
    //   ...authorItems.map(function () {
    //     return $(this).text();
    //   }),
    // ]
    //   .join(",")
    //   .split(",")
    //   .map((val) => String(val).trim())
    //   .filter((item, index, array) => array.indexOf(item) === index)
    //   .sort();

    let _authorItems = ["AFP", "RTL", "RTBF"].sort();

    $(".column-post_meta_authors select, .column-post_meta_authors ul").empty();

    _authorItems.forEach((c, i) => {
      $(".column-post_meta_authors select").append(
        `<option calue="${escape(c)}">${c}</option>`
      );
      $(".column-post_meta_authors ul").append(buildListItem(c, i));
    });
    await waitFor();

    $(".column-post_meta_authors ul li[data-original-index]").on(
      "click",
      function () {
        $(this).toggleClass("selected");
        if ($(this).hasClass("selected")) {
          $(this).find("a").attr("aria-selected", true);
          return;
        } else {
          $(this).find("a").attr("aria-selected", false);
          return;
        }
      }
    );
  }

  let observer;
  function observe() {
    observer = new MutationObserver(async (mutations) => {
      // Table fix - Adds the number of total 'th's as the colspan for the row-details elements.
      let columns = $("#table_1 thead th").length;
      let rowDetails = $("tr.row-detail");
      let width = rowDetails.width();
      if (rowDetails.length) {
        rowDetails.each(function (el, index) {
          $(this).find("td").attr("colspan", columns);
          $(this)
            .find("td")
            .css({ width: `${width}px` });
        });
      }

      // Custom filter fix - OnClear re-builds the element
      let hasText =
        $(".column-post_meta_authors span.filter-option").text() !== "" ||
        $(".column-post_taxonomy_category span.filter-option").text() !== "";

      let hasSelected =
        $(".column-post_meta_authors ul li.selected").length > 0 ||
        $(".column-post_taxonomy_category ul li.selected").length > 0;

      if (!hasText && hasSelected) await buildSelectBox();
    });

    observer.observe(document.body, {
      childList: true,
      subtree: true,
      attributes: false,
      characterData: false,
    });
  }

  if (!$("#table_1").length) {
    if (observer) observer.disconnect();
    return;
  }

  let table = $("#table_1").DataTable();
  await buildSelectBox();
  observe();

  $("#table_1_range_from_6, #table_1_range_to_6").change(function () {
    // console.log("Table will be re-draw cause of number change");
    table.draw();
  });
});
