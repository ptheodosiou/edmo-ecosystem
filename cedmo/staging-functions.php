<?php

// Increase Max Upload Size
@ini_set("upload_max_size", "64M");
@ini_set("post_max_size", "64M");
@ini_set("max_execution_time", "300");

/* Custom Cron Job */
add_action("custom_wp_delete_drafts", "custom_wp_delete_drafts");
function custom_wp_delete_drafts($days_ago)
{
    $defaults = [
        'post_type'      => 'post',
        'orderby'        => 'date',
        'posts_per_page' => -1,
        'post_status' => 'draft',
        'suppress_filters' => 1,
        'lang' => '',
        'date_query' => array(
            array(
                'before' => strval($days_ago)
            )
        ),
        'fields'         => 'ids' // Only return post ID's for performance
    ];

    $query = new WP_Query($defaults);

    if ($query->have_posts()) {
        while ($query->have_posts()) {
            $query->the_post();

            // Delete post
            wp_delete_post(get_the_ID(), true);
        }
    }
}

/* Deletes post's attachments upon post delete */
add_action("before_delete_post", "delete_all_attached_media");
function delete_all_attached_media($post_id)
{
    if (get_post_type($post_id) == "post") {
        $attachments = get_attached_media("", $post_id);

        foreach ($attachments as $attachment) {
            wp_delete_attachment($attachment->ID, "true");
        }
    }
}

/* Custom date format for all languages */
function custom_date_format($post_date)
{
    $post = get_post(get_the_ID());
    return date("d.m.Y", strtotime($post->post_date));
}

add_filter("get_the_date", "custom_date_format");

/* Register template redirect action callback */
add_action("template_redirect", "custom_remove_archives");

/* Remove archives */
function custom_remove_archives()
{
    //If we are on category or tag or date or author archive
    if (is_category() || is_tag() || is_date() || is_author()) {
        wp_redirect(get_permalink(1641));

        // global $wp_query;
        // $wp_query->set_404(); //set to 404 not found page
    }
}

function is_blog()
{
    global $post;
    $posttype = get_post_type($post);
    return (is_archive() ||
        is_author() ||
        is_category() ||
        is_home() ||
        is_single() ||
        is_tag()) &&
        $posttype == "post"
        ? true
        : false;
}

add_filter("the_content", "add_acf_metadata", 1);
function add_acf_metadata($content)
{
    // Check if we're inside a single post
    if (is_blog()) {
        $show_metatags = get_field("show_metatags");
        $uuid = get_field("id");
        $authors = get_field("authors");
        $organization = get_field("organization");
        $img_copyright = get_field("featured_image_copyright");
        $text_copyright = get_field("text_copyright");
        $source = get_field("source");
        $fact_checker = get_field("fact_checker");
        $fact_checker_logo_url = get_field("fact_checker_logo_image");

        $logo =
            isset($fact_checker_logo_url) && !empty($fact_checker_logo_url)
            ? $fact_checker_logo_url
            : $fact_checker;

        if ((bool) $show_metatags == false) {
            return $content;
        } else {
            $top = build_fact_check_metabox__top(
                $img_copyright,
                $text_copyright,
                $authors
            );
            $bottom = build_fact_check_metabox__bottom($logo, $source);

            return $top . $content . $bottom;
        }
    }

    return $content;
}

function build_fact_check_metabox__top(
    $img_copyright,
    $text_copyright,
    $authors
) {
    $img_copyright =
        isset($img_copyright) && !empty($img_copyright) ? $img_copyright : null;
    $text_copyright =
        isset($text_copyright) && !empty($text_copyright)
        ? $text_copyright
        : null;
    $authors = isset($authors) && !empty($authors) ? $authors : null;

    $content = "
    <style>.fact-check-metabox .fact-check-metabox__bottom{display:flex;justify-content:flex-start;align-items:center;gap:10px;padding:10px 0;border:solid thin rgba(128,128,128,.1)}.fact-check-metabox .fact-check-metabox__bottom .fact-check-img-wrapper{width:auto;height:75px;padding:0 20px}.fact-check-metabox .fact-check-metabox__bottom .fact-check-img-wrapper img{width:auto;height:75px;object-fit:cover;object-position:center center}.fact-check-metabox .fact-check-metabox__bottom .fact-check-text{font-family:inherit}.d-none{display:none!important}</style>
    <div class='fact-check-metabox'>
        <div class='fact-check-metabox__top'>
            <p class='img_copyright'>$img_copyright</p>
            <p class='text_copyright'>$text_copyright</p>
            <p class='authors'><b>Author(s):</b> $authors</p>
        </div>
    </div>    
    ";

    return $img_copyright || $text_copyright || $authors ? $content : "";
}

function build_fact_check_metabox__bottom($logo_url, $original_url)
{
    $lang = ICL_LANGUAGE_CODE;
    $orgPublished = "";
    if ($lang == "en") {
        $orgPublished = "Originally published";
    } elseif ($lang == "cs") {
        $orgPublished = "Původně zveřejněno";
    } elseif ($lang == "pl") {
        $orgPublished = "Pierwotnie opublikowane";
    } elseif ($lang == "sk") {
        $orgPublished = "Pôvodne uverejnené";
    } else {
        $orgPublished = "Originally published";
    }

    $here = "";
    if ($lang == "en") {
        $here = "here";
    } elseif ($lang == "cs") {
        $here = "zde";
    } elseif ($lang == "pl") {
        $here = "tutaj";
    } elseif ($lang == "sk") {
        $here = "tu";
    } else {
        $here = "here";
    }

    $logo_url =
        isset($logo_url) && !empty($logo_url)
        ? $logo_url
        : "https://www.gravatar.com/avatar/94d093eda664addd6e450d7e9881bcad?s=75&d=identicon&r=PG";
    $original_link =
        isset($original_link) && !empty($original_link) ? $original_link : "/";
    $content = "
    <style>.fact-check-metabox .fact-check-metabox__bottom{display:flex;justify-content:flex-start;align-items:center;gap:10px;padding:10px 0;border:solid thin rgba(128,128,128,.1)}.fact-check-metabox .fact-check-metabox__bottom .fact-check-img-wrapper{width:auto;height:75px;padding:0 20px}.fact-check-metabox .fact-check-metabox__bottom .fact-check-img-wrapper img{width:auto;height:75px;object-fit:cover;object-position:center center}.fact-check-metabox .fact-check-metabox__bottom .fact-check-text{font-family:inherit}.d-none{display:none!important}</style>
    <div class='fact-check-metabox'>
        <div class='fact-check-metabox__bottom'>
            <div class='fact-check-img-wrapper'>
                <img src='$logo_url' alt='Fact Checker Logo' class='img'/>
            </div>
            <div class='fact-check-text'>$orgPublished <a href='$original_url' target='_blank'>$here</a>.</div>
        </div>
    </div>
    ";

    return $logo_url || $original_link ? $content : "";
}

// Custom column in Posts
add_filter("manage_post_posts_columns", function ($columns) {
    return array_merge($columns, [
        "organization" => __("Organization", "textdomain"),
    ]);
});

add_action(
    "manage_post_posts_custom_column",
    function ($column_key, $post_id) {
        if ($column_key == "organization") {
            $organization = get_post_meta($post_id, "organization", true);
            echo !empty($organization)
                ? sprintf(__("%s", "textdomain"), $organization)
                : __("—", "textdomain");
        }
    },
    10,
    2
);

add_filter("manage_edit-post_sortable_columns", function ($columns) {
    $columns["organization"] = "organization";
    return $columns;
});

add_action("pre_get_posts", function ($query) {
    if (!is_admin()) {
        return;
    }

    $orderby = $query->get("orderby");
    if ($orderby == "organization") {
        $query->set("orderby", "organization");
        $query->set("meta_key", "organization");
    }
});

define("THEGEM_CRYPTOCOIN_CHILD", 1);

add_filter("thegem_icon_userpack_enabled", function () {
    return true;
});

function thegem_child_scripts()
{
    wp_register_style(
        "icons-userpack",
        get_stylesheet_directory_uri() . "/css/icons-userpack.css"
    );
    wp_enqueue_style("icons-userpack");
    wp_enqueue_script(
        "three",
        "//cdnjs.cloudflare.com/ajax/libs/three.js/r72/three.min.js"
    );
    wp_enqueue_script(
        "tweenmax",
        "//cdnjs.cloudflare.com/ajax/libs/gsap/1.18.0/TweenMax.min.js"
    );
}
add_action("wp_enqueue_scripts", "thegem_child_scripts");

function thegem_child_admin_scripts()
{
    wp_register_style(
        "icons-userpack",
        get_stylesheet_directory_uri() . "/css/icons-userpack.css"
    );
}
add_action("admin_enqueue_scripts", "thegem_child_admin_scripts");

function thegem_userpack_icons_info_link($link, $pack)
{
    if ($pack == "userpack") {
        return esc_url(
            get_stylesheet_directory_uri() . "/fonts/icons-list-userpack.html"
        );
    }
    return $link;
}

function thegem_child_init()
{
    add_filter(
        "thegem_user_icons_info_link",
        "thegem_userpack_icons_info_link",
        10,
        2
    );
    wp_enqueue_style("icons-userpack");
}

add_action("after_setup_theme", "thegem_child_init");
add_action("wp_enqueue_scripts", "enqueue_load_fa");
function enqueue_load_fa()
{
    wp_enqueue_style(
        "load-fa",
        "//maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"
    );
}

add_filter(
    "thegem_default_theme_options",
    "thegem_cryptocoin_default_theme_options"
);
function thegem_cryptocoin_default_theme_options()
{
    return [
        "theme_version" => "3.3.0",
        "page_layout_style" => "fullwidth",
        "page_padding_top" => "0",
        "page_padding_bottom" => "0",
        "page_padding_left" => "0",
        "page_padding_right" => "0",
        "disable_uppercase_font" => "1",
        "disable_smooth_scroll" => "1",
        "logo_width" => "176",
        "small_logo_width" => "141",
        "logo" =>
        get_stylesheet_directory_uri() .
            "/images/GemCoin-v1-Logo-3x-Dark.png",
        "small_logo" =>
        get_stylesheet_directory_uri() .
            "/images/GemCoin-v1-Logo-0.8x3-Dark.png",
        "logo_light" =>
        get_stylesheet_directory_uri() . "/images/GemCoin-v1-Logo-3x.png",
        "small_logo_light" =>
        get_stylesheet_directory_uri() .
            "/images/GemCoin-v1-Logo-0.8x3.png",
        "favicon" => get_template_directory_uri() . "/images/favicon.ico",
        "preloader_style" => "preloader-4",
        "custom_css" => '@media (max-width: 767px) {
	h1,
	.title-h1,
	.page-title-title h1 {
		font-size: 50px;
		line-height: 60px;
	}
}
@media (max-width: 879px) {
	h2,
	.title-h2,
	.pricing-table-style-5 .pricing-price-title,
	.pricing-table-style-6 .pricing-price-title,
	.pricing-table-style-6 .pricing-price-subtitle,
	h3.comment-reply-title,
	.pricing-table-style-2 .pricing-price-title {
		font-size: 45px;
	}
}
@media (max-width: 879px) {
	h2,
	.title-h2,
	.pricing-table-style-6 .pricing-price-title,
	h3.comment-reply-title {
		line-height: 43px;
	}
}
@media (max-width: 599px) {
	.title-xlarge {
		font-size: 42px;
		line-height: 54px;
	}
}
@media (max-width: 400px) {
	.title-xlarge {
		font-size: 43px;
		line-height: 47px;
	}
}


body #page-preloader .page-preloader-spin,
body .block-preloader .page-preloader-spin,
body .preloader-spin {
border: 2px solid transparent;
border-top-color: #00d2d4;
}
body .preloader-spin {
border-top-color: #00d2d4;
}
body #page-preloader .page-preloader-spin:before,
body .block-preloader .page-preloader-spin:before,
body .preloader-spin:before {
border: 2px solid transparent;
border-top-color: #ffcd26;
}
body .preloader-spin:before {
border-top-color: #ffcd26;
}
body #page-preloader .page-preloader-spin:after,
body .block-preloader .page-preloader-spin:after,
body .preloader-spin:after {
border: 2px solid transparent;
border-top-color: #4c7199;
}
body .preloader-spin:after {
border-top-color: #4c7199;
}',
        "custom_js" => "",
        "portfolio_rewrite_slug" => "",
        "news_rewrite_slug" => "",
        "404_page" => "",
        "size_guide_image" => "",
        "product_quick_view" => "",
        "products_pagination" => "",
        "catalog_view" => "",
        "checkout_type" => "",
        "hamburger_menu_cart_position" => "",
        "product_title_listing_font_family" => "Montserrat",
        "product_title_listing_font_style" => "700",
        "product_title_listing_font_sets" => "latin,latin-ext",
        "product_title_listing_font_size" => "16",
        "product_title_listing_line_height" => "25",
        "product_title_page_font_family" => "Montserrat UltraLight",
        "product_title_page_font_style" => "regular",
        "product_title_page_font_sets" => "latin,latin-ext",
        "product_title_page_font_size" => "28",
        "product_title_page_line_height" => "42",
        "product_title_widget_font_family" => "Source Sans Pro",
        "product_title_widget_font_style" => "regular",
        "product_title_widget_font_sets" => "latin,latin-ext",
        "product_title_widget_font_size" => "16",
        "product_title_widget_line_height" => "25",
        "product_title_cart_font_family" => "Source Sans Pro",
        "product_title_cart_font_style" => "regular",
        "product_title_cart_font_sets" => "latin,latin-ext",
        "product_title_cart_font_size" => "16",
        "product_title_cart_line_height" => "25",
        "product_price_listing_font_family" => "Source Sans Pro",
        "product_price_listing_font_style" => "regular",
        "product_price_listing_font_sets" => "latin,latin-ext",
        "product_price_listing_font_size" => "16",
        "product_price_listing_line_height" => "25",
        "product_price_page_font_family" => "Source Sans Pro",
        "product_price_page_font_style" => "300",
        "product_price_page_font_sets" => "latin,latin-ext",
        "product_price_page_font_size" => "36",
        "product_price_page_line_height" => "36",
        "product_price_widget_font_family" => "Source Sans Pro",
        "product_price_widget_font_style" => "300",
        "product_price_widget_font_sets" => "latin,latin-ext",
        "product_price_widget_font_size" => "20",
        "product_price_widget_line_height" => "30",
        "product_price_cart_font_family" => "Source Sans Pro",
        "product_price_cart_font_style" => "300",
        "product_price_cart_font_sets" => "latin,latin-ext",
        "product_price_cart_font_size" => "24",
        "product_price_cart_line_height" => "30",
        "product_title_listing_color" => "#5f727f",
        "product_title_page_color" => "#3c3950",
        "product_title_widget_color" => "#5f727f",
        "product_title_cart_color" => "#00bcd4",
        "product_price_listing_color" => "#00bcd4",
        "product_price_page_color" => "#3c3950",
        "product_price_widget_color" => "#3c3950",
        "product_price_cart_color" => "#3c3950",
        "product_separator_listing_color" => "#000000",
        "header_layout" => "fullwidth",
        "header_style" => "3",
        "mobile_menu_layout" => "default",
        "mobile_menu_layout_style" => "light",
        "logo_position" => "left",
        "menu_appearance_tablet_portrait" => "responsive",
        "menu_appearance_tablet_landscape" => "responsive",
        "hamburger_menu_icon_size" => "",
        "top_area_style" => "2",
        "top_area_alignment" => "justified",
        "top_area_contacts" => "1",
        "top_area_socials" => "1",
        "top_area_button_text" => "Join Now",
        "top_area_button_link" => "#",
        "top_area_disable_fixed" => "1",
        "top_area_disable_mobile" => "1",
        "main_menu_font_family" => "Barlow",
        "main_menu_font_style" => "300",
        "main_menu_font_sets" => "",
        "main_menu_font_size" => "15",
        "main_menu_line_height" => "25",
        "submenu_font_family" => "Barlow",
        "submenu_font_style" => "regular",
        "submenu_font_sets" => "",
        "submenu_font_size" => "16",
        "submenu_line_height" => "20",
        "overlay_menu_font_family" => "Barlow",
        "overlay_menu_font_style" => "700",
        "overlay_menu_font_sets" => "",
        "overlay_menu_font_size" => "32",
        "overlay_menu_line_height" => "64",
        "mobile_menu_font_family" => "Source Sans Pro",
        "mobile_menu_font_style" => "regular",
        "mobile_menu_font_sets" => "",
        "mobile_menu_font_size" => "16",
        "mobile_menu_line_height" => "20",
        "styled_subtitle_font_family" => "Barlow",
        "styled_subtitle_font_style" => "200",
        "styled_subtitle_font_sets" => "",
        "styled_subtitle_font_size" => "21",
        "styled_subtitle_line_height" => "35",
        "h1_font_family" => "Barlow",
        "h1_font_style" => "100",
        "h1_font_sets" => "latin-ext,latin",
        "h1_font_size" => "80",
        "h1_line_height" => "100",
        "h2_font_family" => "Barlow",
        "h2_font_style" => "100",
        "h2_font_sets" => "latin-ext,latin",
        "h2_font_size" => "60",
        "h2_line_height" => "67",
        "h3_font_family" => "Barlow",
        "h3_font_style" => "100",
        "h3_font_sets" => "",
        "h3_font_size" => "45",
        "h3_line_height" => "62",
        "h4_font_family" => "Barlow",
        "h4_font_style" => "100",
        "h4_font_sets" => "",
        "h4_font_size" => "32",
        "h4_line_height" => "46",
        "h5_font_family" => "Barlow",
        "h5_font_style" => "300",
        "h5_font_sets" => "",
        "h5_font_size" => "26",
        "h5_line_height" => "40",
        "h6_font_family" => "Barlow",
        "h6_font_style" => "300",
        "h6_font_sets" => "",
        "h6_font_size" => "21",
        "h6_line_height" => "35",
        "xlarge_title_font_family" => "Barlow",
        "xlarge_title_font_style" => "300",
        "xlarge_title_font_sets" => "latin-ext,latin",
        "xlarge_title_font_size" => "80",
        "xlarge_title_line_height" => "90",
        "light_title_font_family" => "Barlow",
        "light_title_font_style" => "100",
        "light_title_font_sets" => "",
        "body_font_family" => "Barlow",
        "body_font_style" => "regular",
        "body_font_sets" => "",
        "body_font_size" => "16",
        "body_line_height" => "25",
        "widget_title_font_family" => "Baloo",
        "widget_title_font_sets" => "",
        "widget_title_font_size" => "19",
        "widget_title_line_height" => "30",
        "button_font_family" => "Barlow",
        "button_font_style" => "regular",
        "button_font_sets" => "latin-ext,latin",
        "button_thin_font_family" => "Barlow",
        "button_thin_font_style" => "100",
        "button_thin_font_sets" => "",
        "portfolio_title_font_family" => "Barlow",
        "portfolio_title_font_style" => "700",
        "portfolio_title_font_sets" => "",
        "portfolio_title_font_size" => "16",
        "portfolio_title_line_height" => "24",
        "portfolio_description_font_family" => "Barlow",
        "portfolio_description_font_style" => "regular",
        "portfolio_description_font_sets" => "",
        "portfolio_description_font_size" => "16",
        "portfolio_description_line_height" => "24",
        "quickfinder_title_font_family" => "Barlow",
        "quickfinder_title_font_style" => "100",
        "quickfinder_title_font_sets" => "latin-ext,latin",
        "quickfinder_title_font_size" => "24",
        "quickfinder_title_line_height" => "38",
        "quickfinder_title_thin_font_family" => "Barlow",
        "quickfinder_title_thin_font_style" => "100",
        "quickfinder_title_thin_font_sets" => "latin-ext,latin",
        "quickfinder_title_thin_font_size" => "24",
        "quickfinder_title_thin_line_height" => "38",
        "quickfinder_description_font_family" => "Barlow",
        "quickfinder_description_font_style" => "300",
        "quickfinder_description_font_sets" => "",
        "quickfinder_description_font_size" => "16",
        "quickfinder_description_line_height" => "25",
        "gallery_title_font_family" => "Barlow",
        "gallery_title_font_style" => "100",
        "gallery_title_font_sets" => "",
        "gallery_title_font_size" => "24",
        "gallery_title_line_height" => "30",
        "gallery_title_bold_font_family" => "Barlow",
        "gallery_title_bold_font_style" => "regular",
        "gallery_title_bold_font_sets" => "latin,latin-ext",
        "gallery_title_bold_font_size" => "24",
        "gallery_title_bold_line_height" => "31",
        "gallery_description_font_family" => "Barlow",
        "gallery_description_font_style" => "300",
        "gallery_description_font_sets" => "",
        "gallery_description_font_size" => "17",
        "gallery_description_line_height" => "24",
        "testimonial_font_family" => "Barlow",
        "testimonial_font_style" => "300",
        "testimonial_font_sets" => "",
        "testimonial_font_size" => "24",
        "testimonial_line_height" => "36",
        "counter_font_family" => "Barlow",
        "counter_font_style" => "100",
        "counter_font_sets" => "",
        "counter_font_size" => "64",
        "counter_line_height" => "90",
        "woocommerce_price_font_family" => "Barlow",
        "woocommerce_price_font_style" => "regular",
        "woocommerce_price_font_sets" => "",
        "woocommerce_price_font_size" => "26",
        "woocommerce_price_line_height" => "36",
        "slideshow_title_font_family" => "Barlow",
        "slideshow_title_font_style" => "700",
        "slideshow_title_font_sets" => "",
        "slideshow_title_font_size" => "50",
        "slideshow_title_line_height" => "69",
        "slideshow_description_font_family" => "Barlow",
        "slideshow_description_font_style" => "regular",
        "slideshow_description_font_sets" => "",
        "slideshow_description_font_size" => "16",
        "slideshow_description_line_height" => "25",
        "basic_outer_background_color" => "#f0f3f2",
        "top_background_color" => "#ffffff",
        "main_background_color" => "#ffffff",
        "footer_widget_area_background_color" => "#1a1c21",
        "footer_background_color" => "#1a1c21",
        "styled_elements_background_color" => "#f4f6f7",
        "styled_elements_color_1" => "#00d2d4",
        "styled_elements_color_2" => "#99a9b5",
        "styled_elements_color_3" => "#f44336",
        "styled_elements_color_4" => "#393d50",
        "divider_default_color" => "#dfe5e8",
        "box_border_color" => "#dfe5e8",
        "main_menu_level1_color" => "#3c3950",
        "main_menu_level1_background_color" => "",
        "main_menu_level1_hover_color" => "#00d2d3",
        "main_menu_level1_hover_background_color" => "",
        "main_menu_level1_active_color" => "#3c3950",
        "main_menu_level1_active_background_color" => "#3c3950",
        "main_menu_level2_color" => "#5f727f",
        "main_menu_level2_background_color" => "#f4f6f7",
        "main_menu_level2_hover_color" => "#3c3950",
        "main_menu_level2_hover_background_color" => "#ffffff",
        "main_menu_level2_active_color" => "#3c3950",
        "main_menu_level2_active_background_color" => "#ffffff",
        "main_menu_mega_column_title_color" => "#3c3950",
        "main_menu_mega_column_title_hover_color" => "#00d2d3",
        "main_menu_mega_column_title_active_color" => "#00d2d3",
        "main_menu_level3_color" => "#5f727f",
        "main_menu_level3_background_color" => "#ffffff",
        "main_menu_level3_hover_color" => "#ffffff",
        "main_menu_level3_hover_background_color" => "#494c64",
        "main_menu_level3_active_color" => "#00d2d3",
        "main_menu_level3_active_background_color" => "#ffffff",
        "main_menu_level1_light_color" => "#ffffff",
        "main_menu_level1_light_hover_color" => "#00d2d3",
        "main_menu_level1_light_active_color" => "#ffffff",
        "main_menu_level2_border_color" => "#dfe5e8",
        "mega_menu_icons_color" => "",
        "overlay_menu_background_color" => "#212331",
        "overlay_menu_color" => "#ffffff",
        "overlay_menu_hover_color" => "#00d2d4",
        "overlay_menu_active_color" => "#00d2d3",
        "hamburger_menu_icon_color" => "",
        "hamburger_menu_icon_light_color" => "",
        "mobile_menu_button_color" => "",
        "mobile_menu_button_light_color" => "",
        "mobile_menu_background_color" => "",
        "mobile_menu_level1_color" => "#5f727f",
        "mobile_menu_level1_background_color" => "#f4f6f7",
        "mobile_menu_level1_active_color" => "#3c3950",
        "mobile_menu_level1_active_background_color" => "#ffffff",
        "mobile_menu_level2_color" => "#5f727f",
        "mobile_menu_level2_background_color" => "#f4f6f7",
        "mobile_menu_level2_active_color" => "#3c3950",
        "mobile_menu_level2_active_background_color" => "#ffffff",
        "mobile_menu_level3_color" => "#5f727f",
        "mobile_menu_level3_background_color" => "#f4f6f7",
        "mobile_menu_level3_active_color" => "#3c3950",
        "mobile_menu_level3_active_background_color" => "#ffffff",
        "mobile_menu_border_color" => "#dfe5e8",
        "mobile_menu_social_icon_color" => "",
        "mobile_menu_hide_color" => "",
        "top_area_background_color" => "#1a1c21",
        "top_area_border_color" => "#474b61",
        "top_area_separator_color" => "#51546c",
        "top_area_text_color" => "#99a9b5",
        "top_area_link_color" => "#00d2d4",
        "top_area_link_hover_color" => "#ffffff",
        "top_area_button_text_color" => "#03babc",
        "top_area_button_background_color" => "#03babc",
        "top_area_button_hover_text_color" => "#ffffff",
        "top_area_button_hover_background_color" => "#46485c",
        "body_color" => "#5f727f",
        "h1_color" => "#24262e",
        "h2_color" => "#24262e",
        "h3_color" => "#24262e",
        "h4_color" => "#24262e",
        "h5_color" => "#24262e",
        "h6_color" => "#24262e",
        "link_color" => "#00d2d4",
        "hover_link_color" => "#384554",
        "active_link_color" => "#00d2d4",
        "footer_text_color" => "#fff",
        "copyright_text_color" => "#fff",
        "copyright_link_color" => "#ffcd26",
        "title_bar_background_color" => "#6c7cd0",
        "title_bar_text_color" => "#ffffff",
        "date_filter_subtitle_color" => "#99a9b5",
        "system_icons_font" => "#99a3b0",
        "system_icons_font_2" => "#b6c6c9",
        "button_text_basic_color" => "#ffffff",
        "button_text_hover_color" => "#ffffff",
        "button_background_basic_color" => "#b6c6c9",
        "button_background_hover_color" => "#1a1c21",
        "button_outline_text_basic_color" => "#00d2d4",
        "button_outline_text_hover_color" => "#ffffff",
        "button_outline_border_basic_color" => "#00d2d4",
        "widget_title_color" => "#3c3950",
        "widget_link_color" => "#5f727f",
        "widget_hover_link_color" => "#00bcd4",
        "widget_active_link_color" => "#384554",
        "footer_widget_title_color" => "#feffff",
        "footer_widget_text_color" => "#99a9b5",
        "footer_widget_link_color" => "#99a9b5",
        "footer_widget_hover_link_color" => "#00bcd4",
        "footer_widget_active_link_color" => "#00bcd4",
        "portfolio_title_color" => "#5f727f",
        "portfolio_description_color" => "#5f727f",
        "portfolio_date_color" => "#99a9b5",
        "gallery_caption_background_color" => "#000000",
        "gallery_title_color" => "#ffffff",
        "gallery_description_color" => "#ffffff",
        "slideshow_arrow_background" => "#394050",
        "slideshow_arrow_hover_background" => "#00d2d4",
        "slideshow_arrow_color" => "#ffffff",
        "sliders_arrow_color" => "#3c3950",
        "sliders_arrow_background_color" => "#b6c6c9",
        "sliders_arrow_hover_color" => "#ffffff",
        "sliders_arrow_background_hover_color" => "#00d2d4",
        "hover_effect_default_color" => "#00d2d4",
        "hover_effect_zooming_blur_color" => "#ffffff",
        "hover_effect_horizontal_sliding_color" => "#46485c",
        "hover_effect_vertical_sliding_color" => "#f44336",
        "quickfinder_title_color" => "#4c5867",
        "quickfinder_description_color" => "#5f727f",
        "bullets_symbol_color" => "#5f727f",
        "icons_symbol_color" => "#91a0ac",
        "pagination_basic_color" => "#99a9b5",
        "pagination_basic_background_color" => "#ffffff",
        "pagination_hover_color" => "#00d2d4",
        "pagination_active_color" => "#3c3950",
        "mini_pagination_color" => "#b6c6c9",
        "mini_pagination_active_color" => "#00d2d4",
        "form_elements_background_color" => "#f4f6f7",
        "form_elements_text_color" => "#3c3950",
        "form_elements_border_color" => "#dfe5e8",
        "breadcrumbs_default_color" => "",
        "breadcrumbs_active_color" => "",
        "breadcrumbs_hover_color" => "",
        "basic_outer_background_image" => "",
        "top_background_image" => "",
        "top_area_background_image" => "",
        "main_background_image" => "",
        "footer_background_image" => "",
        "footer_widget_area_background_image" => "",
        "slider_effect" => "random",
        "slider_slices" => "15",
        "slider_boxCols" => "8",
        "slider_boxRows" => "4",
        "slider_animSpeed" => "5",
        "slider_pauseTime" => "20",
        "slider_directionNav" => "1",
        "slider_controlNav" => "1",
        "show_author" => "1",
        "excerpt_length" => "20",
        "footer_active" => "1",
        "footer_html" => "&copy; copyright 2018",
        "custom_footer" => "",
        "contacts_address" =>
        "908 New Hampshire Avenue #100, Washington, DC 20037, United States",
        "contacts_phone" => "+1 916-875-2235",
        "contacts_fax" => "+1 916-875-2235",
        "contacts_email" => "info@domain.tld",
        "contacts_website" => "www.codex-themes.com",
        "top_area_contacts_address" => "",
        "top_area_contacts_phone" => "",
        "top_area_contacts_fax" => "",
        "top_area_contacts_email" => "",
        "top_area_contacts_website" => "",
        "twitter_active" => "1",
        "facebook_active" => "1",
        "linkedin_active" => "1",
        "googleplus_active" => "1",
        "stumbleupon_active" => "1",
        "rss_active" => "1",
        "instagram_active" => "1",
        "youtube_active" => "1",
        "flickr_active" => "1",
        "twitter_link" => "#",
        "facebook_link" => "#",
        "linkedin_link" => "#",
        "googleplus_link" => "#",
        "stumbleupon_link" => "#",
        "rss_link" => "#",
        "vimeo_link" => "#",
        "instagram_link" => "#",
        "pinterest_link" => "#",
        "youtube_link" => "#",
        "flickr_link" => "#",
        "show_social_icons" => "1",
    ];
}

add_filter(
    "thegem_default_skins_options",
    "thegem_cryptocoin_default_skins_options"
);
function thegem_cryptocoin_default_skins_options()
{
    return [
        "light" => [
            "product_title_listing_font_family" => "Montserrat",
            "product_title_listing_font_style" => "700",
            "product_title_listing_font_sets" => "latin,latin-ext",
            "product_title_listing_font_size" => "16",
            "product_title_listing_line_height" => "25",
            "product_title_page_font_family" => "Montserrat UltraLight",
            "product_title_page_font_style" => "regular",
            "product_title_page_font_sets" => "latin,latin-ext",
            "product_title_page_font_size" => "28",
            "product_title_page_line_height" => "42",
            "product_title_widget_font_family" => "Source Sans Pro",
            "product_title_widget_font_style" => "regular",
            "product_title_widget_font_sets" => "latin,latin-ext",
            "product_title_widget_font_size" => "16",
            "product_title_widget_line_height" => "25",
            "product_title_cart_font_family" => "Source Sans Pro",
            "product_title_cart_font_style" => "regular",
            "product_title_cart_font_sets" => "latin,latin-ext",
            "product_title_cart_font_size" => "16",
            "product_title_cart_line_height" => "25",
            "product_price_listing_font_family" => "Source Sans Pro",
            "product_price_listing_font_style" => "regular",
            "product_price_listing_font_sets" => "latin,latin-ext",
            "product_price_listing_font_size" => "16",
            "product_price_listing_line_height" => "25",
            "product_price_page_font_family" => "Source Sans Pro",
            "product_price_page_font_style" => "300",
            "product_price_page_font_sets" => "latin,latin-ext",
            "product_price_page_font_size" => "36",
            "product_price_page_line_height" => "36",
            "product_price_widget_font_family" => "Source Sans Pro",
            "product_price_widget_font_style" => "300",
            "product_price_widget_font_sets" => "latin,latin-ext",
            "product_price_widget_font_size" => "20",
            "product_price_widget_line_height" => "30",
            "product_price_cart_font_family" => "Source Sans Pro",
            "product_price_cart_font_style" => "300",
            "product_price_cart_font_sets" => "latin,latin-ext",
            "product_price_cart_font_size" => "24",
            "product_price_cart_line_height" => "30",
            "product_title_listing_color" => "#5f727f",
            "product_title_page_color" => "#3c3950",
            "product_title_widget_color" => "#5f727f",
            "product_title_cart_color" => "#00bcd4",
            "product_price_listing_color" => "#00bcd4",
            "product_price_page_color" => "#3c3950",
            "product_price_widget_color" => "#3c3950",
            "product_price_cart_color" => "#3c3950",
            "product_separator_listing_color" => "#000000",
            "header_layout" => "fullwidth",
            "header_style" => "3",
            "mobile_menu_layout" => "default",
            "mobile_menu_layout_style" => "light",
            "logo_position" => "left",
            "menu_appearance_tablet_portrait" => "responsive",
            "menu_appearance_tablet_landscape" => "responsive",
            "hamburger_menu_icon_size" => "",
            "top_area_style" => "2",
            "top_area_alignment" => "justified",
            "top_area_contacts" => "1",
            "top_area_socials" => "1",
            "top_area_button_text" => "Join Now",
            "top_area_button_link" => "#",
            "top_area_disable_fixed" => "1",
            "top_area_disable_mobile" => "1",
            "main_menu_font_family" => "Barlow",
            "main_menu_font_style" => "300",
            "main_menu_font_sets" => "",
            "main_menu_font_size" => "15",
            "main_menu_line_height" => "25",
            "submenu_font_family" => "Barlow",
            "submenu_font_style" => "regular",
            "submenu_font_sets" => "",
            "submenu_font_size" => "16",
            "submenu_line_height" => "20",
            "overlay_menu_font_family" => "Barlow",
            "overlay_menu_font_style" => "700",
            "overlay_menu_font_sets" => "",
            "overlay_menu_font_size" => "32",
            "overlay_menu_line_height" => "64",
            "mobile_menu_font_family" => "Source Sans Pro",
            "mobile_menu_font_style" => "regular",
            "mobile_menu_font_sets" => "",
            "mobile_menu_font_size" => "16",
            "mobile_menu_line_height" => "20",
            "styled_subtitle_font_family" => "Barlow",
            "styled_subtitle_font_style" => "200",
            "styled_subtitle_font_sets" => "",
            "styled_subtitle_font_size" => "21",
            "styled_subtitle_line_height" => "35",
            "h1_font_family" => "Barlow",
            "h1_font_style" => "100",
            "h1_font_sets" => "latin-ext,latin",
            "h1_font_size" => "80",
            "h1_line_height" => "100",
            "h2_font_family" => "Barlow",
            "h2_font_style" => "100",
            "h2_font_sets" => "latin-ext,latin",
            "h2_font_size" => "60",
            "h2_line_height" => "67",
            "h3_font_family" => "Barlow",
            "h3_font_style" => "100",
            "h3_font_sets" => "",
            "h3_font_size" => "45",
            "h3_line_height" => "62",
            "h4_font_family" => "Barlow",
            "h4_font_style" => "100",
            "h4_font_sets" => "",
            "h4_font_size" => "32",
            "h4_line_height" => "46",
            "h5_font_family" => "Barlow",
            "h5_font_style" => "300",
            "h5_font_sets" => "",
            "h5_font_size" => "26",
            "h5_line_height" => "40",
            "h6_font_family" => "Barlow",
            "h6_font_style" => "300",
            "h6_font_sets" => "",
            "h6_font_size" => "21",
            "h6_line_height" => "35",
            "xlarge_title_font_family" => "Barlow",
            "xlarge_title_font_style" => "300",
            "xlarge_title_font_sets" => "latin-ext,latin",
            "xlarge_title_font_size" => "80",
            "xlarge_title_line_height" => "90",
            "light_title_font_family" => "Barlow",
            "light_title_font_style" => "100",
            "light_title_font_sets" => "",
            "body_font_family" => "Barlow",
            "body_font_style" => "regular",
            "body_font_sets" => "",
            "body_font_size" => "16",
            "body_line_height" => "25",
            "widget_title_font_family" => "Baloo",
            "widget_title_font_sets" => "",
            "widget_title_font_size" => "19",
            "widget_title_line_height" => "30",
            "button_font_family" => "Barlow",
            "button_font_style" => "regular",
            "button_font_sets" => "latin-ext,latin",
            "button_thin_font_family" => "Barlow",
            "button_thin_font_style" => "100",
            "button_thin_font_sets" => "",
            "portfolio_title_font_family" => "Barlow",
            "portfolio_title_font_style" => "700",
            "portfolio_title_font_sets" => "",
            "portfolio_title_font_size" => "16",
            "portfolio_title_line_height" => "24",
            "portfolio_description_font_family" => "Barlow",
            "portfolio_description_font_style" => "regular",
            "portfolio_description_font_sets" => "",
            "portfolio_description_font_size" => "16",
            "portfolio_description_line_height" => "24",
            "quickfinder_title_font_family" => "Barlow",
            "quickfinder_title_font_style" => "100",
            "quickfinder_title_font_sets" => "latin-ext,latin",
            "quickfinder_title_font_size" => "24",
            "quickfinder_title_line_height" => "38",
            "quickfinder_title_thin_font_family" => "Barlow",
            "quickfinder_title_thin_font_style" => "100",
            "quickfinder_title_thin_font_sets" => "latin-ext,latin",
            "quickfinder_title_thin_font_size" => "24",
            "quickfinder_title_thin_line_height" => "38",
            "quickfinder_description_font_family" => "Barlow",
            "quickfinder_description_font_style" => "300",
            "quickfinder_description_font_sets" => "",
            "quickfinder_description_font_size" => "16",
            "quickfinder_description_line_height" => "25",
            "gallery_title_font_family" => "Barlow",
            "gallery_title_font_style" => "100",
            "gallery_title_font_sets" => "",
            "gallery_title_font_size" => "24",
            "gallery_title_line_height" => "30",
            "gallery_title_bold_font_family" => "Barlow",
            "gallery_title_bold_font_style" => "regular",
            "gallery_title_bold_font_sets" => "latin,latin-ext",
            "gallery_title_bold_font_size" => "24",
            "gallery_title_bold_line_height" => "31",
            "gallery_description_font_family" => "Barlow",
            "gallery_description_font_style" => "300",
            "gallery_description_font_sets" => "",
            "gallery_description_font_size" => "17",
            "gallery_description_line_height" => "24",
            "testimonial_font_family" => "Barlow",
            "testimonial_font_style" => "300",
            "testimonial_font_sets" => "",
            "testimonial_font_size" => "24",
            "testimonial_line_height" => "36",
            "counter_font_family" => "Barlow",
            "counter_font_style" => "100",
            "counter_font_sets" => "",
            "counter_font_size" => "64",
            "counter_line_height" => "90",
            "woocommerce_price_font_family" => "Barlow",
            "woocommerce_price_font_style" => "regular",
            "woocommerce_price_font_sets" => "",
            "woocommerce_price_font_size" => "26",
            "woocommerce_price_line_height" => "36",
            "slideshow_title_font_family" => "Barlow",
            "slideshow_title_font_style" => "700",
            "slideshow_title_font_sets" => "",
            "slideshow_title_font_size" => "50",
            "slideshow_title_line_height" => "69",
            "slideshow_description_font_family" => "Barlow",
            "slideshow_description_font_style" => "regular",
            "slideshow_description_font_sets" => "",
            "slideshow_description_font_size" => "16",
            "slideshow_description_line_height" => "25",
            "basic_outer_background_color" => "#f0f3f2",
            "top_background_color" => "#ffffff",
            "main_background_color" => "#ffffff",
            "footer_widget_area_background_color" => "#1a1c21",
            "footer_background_color" => "#1a1c21",
            "styled_elements_background_color" => "#f4f6f7",
            "styled_elements_color_1" => "#00d2d4",
            "styled_elements_color_2" => "#99a9b5",
            "styled_elements_color_3" => "#f44336",
            "styled_elements_color_4" => "#393d50",
            "divider_default_color" => "#dfe5e8",
            "box_border_color" => "#dfe5e8",
            "main_menu_level1_color" => "#3c3950",
            "main_menu_level1_background_color" => "",
            "main_menu_level1_hover_color" => "#00d2d3",
            "main_menu_level1_hover_background_color" => "",
            "main_menu_level1_active_color" => "#3c3950",
            "main_menu_level1_active_background_color" => "#3c3950",
            "main_menu_level2_color" => "#5f727f",
            "main_menu_level2_background_color" => "#f4f6f7",
            "main_menu_level2_hover_color" => "#3c3950",
            "main_menu_level2_hover_background_color" => "#ffffff",
            "main_menu_level2_active_color" => "#3c3950",
            "main_menu_level2_active_background_color" => "#ffffff",
            "main_menu_mega_column_title_color" => "#3c3950",
            "main_menu_mega_column_title_hover_color" => "#00d2d3",
            "main_menu_mega_column_title_active_color" => "#00d2d3",
            "main_menu_level3_color" => "#5f727f",
            "main_menu_level3_background_color" => "#ffffff",
            "main_menu_level3_hover_color" => "#ffffff",
            "main_menu_level3_hover_background_color" => "#494c64",
            "main_menu_level3_active_color" => "#00d2d3",
            "main_menu_level3_active_background_color" => "#ffffff",
            "main_menu_level1_light_color" => "#ffffff",
            "main_menu_level1_light_hover_color" => "#00d2d3",
            "main_menu_level1_light_active_color" => "#ffffff",
            "main_menu_level2_border_color" => "#dfe5e8",
            "mega_menu_icons_color" => "",
            "overlay_menu_background_color" => "#212331",
            "overlay_menu_color" => "#ffffff",
            "overlay_menu_hover_color" => "#00d2d4",
            "overlay_menu_active_color" => "#00d2d3",
            "hamburger_menu_icon_color" => "",
            "hamburger_menu_icon_light_color" => "",
            "mobile_menu_button_color" => "",
            "mobile_menu_button_light_color" => "",
            "mobile_menu_background_color" => "",
            "mobile_menu_level1_color" => "#5f727f",
            "mobile_menu_level1_background_color" => "#f4f6f7",
            "mobile_menu_level1_active_color" => "#3c3950",
            "mobile_menu_level1_active_background_color" => "#ffffff",
            "mobile_menu_level2_color" => "#5f727f",
            "mobile_menu_level2_background_color" => "#f4f6f7",
            "mobile_menu_level2_active_color" => "#3c3950",
            "mobile_menu_level2_active_background_color" => "#ffffff",
            "mobile_menu_level3_color" => "#5f727f",
            "mobile_menu_level3_background_color" => "#f4f6f7",
            "mobile_menu_level3_active_color" => "#3c3950",
            "mobile_menu_level3_active_background_color" => "#ffffff",
            "mobile_menu_border_color" => "#dfe5e8",
            "mobile_menu_social_icon_color" => "",
            "mobile_menu_hide_color" => "",
            "top_area_background_color" => "#1a1c21",
            "top_area_border_color" => "#474b61",
            "top_area_separator_color" => "#51546c",
            "top_area_text_color" => "#99a9b5",
            "top_area_link_color" => "#00d2d4",
            "top_area_link_hover_color" => "#ffffff",
            "top_area_button_text_color" => "#03babc",
            "top_area_button_background_color" => "#03babc",
            "top_area_button_hover_text_color" => "#ffffff",
            "top_area_button_hover_background_color" => "#46485c",
            "body_color" => "#5f727f",
            "h1_color" => "#24262e",
            "h2_color" => "#24262e",
            "h3_color" => "#24262e",
            "h4_color" => "#24262e",
            "h5_color" => "#24262e",
            "h6_color" => "#24262e",
            "link_color" => "#00d2d4",
            "hover_link_color" => "#384554",
            "active_link_color" => "#00d2d4",
            "footer_text_color" => "#fff",
            "copyright_text_color" => "#fff",
            "copyright_link_color" => "#ffcd26",
            "title_bar_background_color" => "#6c7cd0",
            "title_bar_text_color" => "#ffffff",
            "date_filter_subtitle_color" => "#99a9b5",
            "system_icons_font" => "#99a3b0",
            "system_icons_font_2" => "#b6c6c9",
            "button_text_basic_color" => "#ffffff",
            "button_text_hover_color" => "#ffffff",
            "button_background_basic_color" => "#b6c6c9",
            "button_background_hover_color" => "#1a1c21",
            "button_outline_text_basic_color" => "#00d2d4",
            "button_outline_text_hover_color" => "#ffffff",
            "button_outline_border_basic_color" => "#00d2d4",
            "widget_title_color" => "#3c3950",
            "widget_link_color" => "#5f727f",
            "widget_hover_link_color" => "#00bcd4",
            "widget_active_link_color" => "#384554",
            "footer_widget_title_color" => "#feffff",
            "footer_widget_text_color" => "#99a9b5",
            "footer_widget_link_color" => "#99a9b5",
            "footer_widget_hover_link_color" => "#00bcd4",
            "footer_widget_active_link_color" => "#00bcd4",
            "portfolio_title_color" => "#5f727f",
            "portfolio_description_color" => "#5f727f",
            "portfolio_date_color" => "#99a9b5",
            "gallery_caption_background_color" => "#000000",
            "gallery_title_color" => "#ffffff",
            "gallery_description_color" => "#ffffff",
            "slideshow_arrow_background" => "#394050",
            "slideshow_arrow_hover_background" => "#00d2d4",
            "slideshow_arrow_color" => "#ffffff",
            "sliders_arrow_color" => "#3c3950",
            "sliders_arrow_background_color" => "#b6c6c9",
            "sliders_arrow_hover_color" => "#ffffff",
            "sliders_arrow_background_hover_color" => "#00d2d4",
            "hover_effect_default_color" => "#00d2d4",
            "hover_effect_zooming_blur_color" => "#ffffff",
            "hover_effect_horizontal_sliding_color" => "#46485c",
            "hover_effect_vertical_sliding_color" => "#f44336",
            "quickfinder_title_color" => "#4c5867",
            "quickfinder_description_color" => "#5f727f",
            "bullets_symbol_color" => "#5f727f",
            "icons_symbol_color" => "#91a0ac",
            "pagination_basic_color" => "#99a9b5",
            "pagination_basic_background_color" => "#ffffff",
            "pagination_hover_color" => "#00d2d4",
            "pagination_active_color" => "#3c3950",
            "mini_pagination_color" => "#b6c6c9",
            "mini_pagination_active_color" => "#00d2d4",
            "form_elements_background_color" => "#f4f6f7",
            "form_elements_text_color" => "#3c3950",
            "form_elements_border_color" => "#dfe5e8",
            "breadcrumbs_default_color" => "",
            "breadcrumbs_active_color" => "",
            "breadcrumbs_hover_color" => "",
            "basic_outer_background_image" => "",
            "top_background_image" => "",
            "top_area_background_image" => "",
            "main_background_image" => "",
            "footer_background_image" => "",
            "footer_widget_area_background_image" => "",
        ],
    ];
}
