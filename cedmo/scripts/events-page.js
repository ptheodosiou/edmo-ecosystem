jQuery(document).ready(async function ($) {
  const waitFor = (ms = 500) => new Promise((r) => setTimeout(r, ms));

  const waitForElement = (selector) => {
    return new Promise((resolve) => {
      if (document.querySelectorAll(selector)) {
        return resolve(document.querySelectorAll(selector));
      }

      const observer = new MutationObserver((mutations) => {
        if (document.querySelectorAll(selector)) {
          resolve(document.querySelectorAll(selector));
          observer.disconnect();
        }
      });

      observer.observe(document.body, {
        childList: true,
        subtree: true,
      });
    });
  };

  const isSpecificPage = (pageRegex) => {
    if (!pageRegex) return false;
    return !!window.location.toString().match(pageRegex);
  };

  if (!isSpecificPage("/events/")) return;

  // Updates the dates of a specific event
  waitForElement(
    ".post .item-post-container .item-post .post-title .entry-title .entry-title-date"
  )
    .then((elements) => {
      let el = [...elements].filter((element) => {
        const _text = $(element).text();
        return _text.includes("08 Dec:");
      });
      el = el[0];
      $(el).text("08-09 Dec: ");
    })
    .catch((error) => {
      console.log("Element not exists");
      console.log(error.toString());
      console.log(error);
    });
});
