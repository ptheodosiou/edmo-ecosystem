jQuery(document).ready(async function ($) {
  const waitFor = (ms = 500) => new Promise((r) => setTimeout(r, ms));

  const waitForElement = (selector) => {
    return new Promise((resolve) => {
      if (document.querySelectorAll(selector)) {
        return resolve(document.querySelectorAll(selector));
      }

      const observer = new MutationObserver((mutations) => {
        if (document.querySelectorAll(selector)) {
          resolve(document.querySelectorAll(selector));
          observer.disconnect();
        }
      });

      observer.observe(document.body, {
        childList: true,
        subtree: true,
      });
    });
  };

  waitForElement(".socials-item")
    .then((elements) => {
      let _elements = [...elements];

      // Items to be deleted
      const _delete = ["Pinterest", "Tumblr", "Reddit"];

      _elements.forEach((el) => {
        const title = $(el).attr("title");
        if (_delete.includes(title)) $(el).remove();
      });
    })
    .catch((error) => {
      console.log("Element not exists");
      console.log(error.toString());
      console.log(error);
    });
});
