<?php
/*
 * Template Name: Custom template
 * Description: For testing purposes
*/

$defaults = [
    'post_type'      => 'post',
    'orderby'        => 'date',
    'posts_per_page' => -1,
    'post_status' => 'draft',
    'suppress_filters' => 1,
    'lang' => '',
    'date_query' => array(
        array(
            'before' => '39 days ago'
        )
    )
    // 'fields'         => 'ids' // Only return post ID's for performance
];

// $query = new WP_Query(array_merge($date_query, $defaults));
$query = new WP_Query($defaults);

if ($query->have_posts()) {
    while ($query->have_posts()) {
        $query->the_post();
        $attachments = get_attached_media('', get_the_ID());

        // Delete the attachments first
        foreach ($attachments as $attachment) {
            wp_delete_attachment($attachment->ID, true);
        }

        // Delete post
        wp_delete_post(get_the_ID(), true);

        echo "<pre>" . print_r(get_the_ID(), true) . "</pre>";
    }
}
