# WordPress Post

## Login

Login to website: http://cedmohub-staging.eu/wp-admin/

Check your email for regenerating a new password. (Check spam folder also).

## Posts

On the left sidebar, you find the quick menu of the control panel. Go and select **Posts**.

You are on the **Posts** page now. Here, you find all the created posts. Through this page, you can **create/edit/delete** a post.

Ensure that you are in the correct language version of the website.

![language-selection](/home/thpadelis/Desktop/language-selection.png)

### Create Post

Either on the left sidebar or on top of the page, there is an **Add New** button. By clicking this button, you will be transferred to the page that will help you to create a new post.

Following, these are the most usual step for creating a post

1. Add Title
2. Write the content for the post
   - Classic Editor
     With **classic editor** you have the very basic options of a rich text editor.
   - Backend Editor
     With **backed editor** you have all the options from the **classic one** plus that you can add custom CSS and more through its own components. For example, you can add images with rounded corners and on click event (open link and more), or style the content as a two-column grid and more. Here is a YouTube video that you might find useful to get started with the **WPBakery editor**.
     - [Basic tutorial](https://www.youtube.com/watch?v=MXQKH66M3Wk)
3. Page options
   In this step, you can select if this post, will be featured on the home page. 
   Posts Settings > Check the **Featured Post** option.
   Leave the rest options as they are.
4. Fact Check
   These are meta info for the post. They are optional fields but you should always fill them in, for better search options and more.
   - GUID | The unique identifier of the post referring to your website - Usually an 8 length number
   - Authors | Authors of the post
   - Date of Publication | The date the post was published
   - Theme | Field name might be updated | The category of the post (ex. Environment)
   - Categories | Generic categories - free text
   - Featured image | Link to the image you want to add to your post. Same thing with the **Featured image** on the step **8**.
5. Page Widgets
   Edit only the fields in the red box.
   - Recent Posts Extended > Limit to Category | Select the categories you want and post with the selected categories, which will be available in the right sidebar of the post. (in preview mode)
6. Language
   Select the language of the post. If you choose **Czech**, this post will only be available for the **Czech** version of the website. You can, later on, translate this post to another language.
7. Categories
   Select the categories that your post will have.
8. Featured Image
   Here you can upload the featured image you want for your post. This image will be on the top of the post and in the areas of the website that previews a post. 
9. Publish
   You can either **Save Draft**, **Publish** or **Schedule a post**.
   For scheduling a post you click on the **edit** link and select the date you want to post it. Then, you simple click **Publish**.
   ![schedule-post](/home/thpadelis/Desktop/schedule-post.png)

### Edit post

In the **Posts** page, where all the posts are available, you can click on the **edit** link of the post you want to edit. You will be transferred on the **Edit Post** page. Again, here you can follow the steps of the creation of a post, in order to edit the fields of your post.

### Delete post

In the **Posts** page, where all the posts are available, you can click on the **trash** link of the post you want to delete.