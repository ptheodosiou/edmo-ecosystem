jQuery(document).ready(async function($) {
  const waitFor = (ms = 500) => new Promise((r) => setTimeout(r, ms));
  const shouldRun =
    window.location.pathname === "/research-activities/" ||
    window.location.pathname.includes("/research-activities/");

  if (!shouldRun) return;

  function buildListItem(value, index) {
    return `<li data-original-index="${index}"><a tabindex="${index}" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">${value}</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>`;
  }

  async function buildSelectBox() {
    await waitFor();

    // Country in which it is based
    let cbItems = $(".column-country select option");
    let _cbItems = [
        ...cbItems.map(function() {
          return $(this).text();
        }),
      ]
      .join(";")
      .split(";")
      .map((val) => String(val).trim())
      .filter((item, index, array) => array.indexOf(item) === index)
      .sort();

    $(".column-country select, .column-country ul").empty();

    _cbItems.forEach((c, i) => {
      $(".column-country select").append(
        `<option calue="${escape(c)}">${c}</option>`
      );
      $(".column-country ul").append(buildListItem(c, i));
    });
    await waitFor();

    $(".column-country ul li[data-original-index]").on("click", function() {
      $(this).toggleClass("selected");
      if ($(this).hasClass("selected")) {
        $(this).find("a").attr("aria-selected", true);
        return;
      } else {
        $(this).find("a").attr("aria-selected", false);
        return;
      }
    });
  }

  const fixTableEntries = (selector = "table tbody tr td.column-link-to-organisationresearch-unit") => {
    const elements = $(selector);
    if (!elements.length) return;
    for (let i = 0; i < elements.length; i++) {
      const element = elements[i];

      const _src = $(element).find("a").attr("href");
      if (_src && !!_src.match(/; /g)) {
        const links = _src.split(";").map(x => x.trim());
        let updatedHTMLElements = [];
        for (let i = 0; i < links.length; i++) {
          const link = links[i];
          const newLink = `<a href="${link}" rel="nofollow" target="_blank">${link}</a>`;
          updatedHTMLElements.push(newLink);
        }
        $(element).html(updatedHTMLElements.join("<br>"));
      }
    }
  }

  const waitForElement = (selector) => {
    return new Promise((resolve) => {
      if (document.querySelector(selector)) {
        return resolve(document.querySelector(selector));
      }

      const observer = new MutationObserver((mutations) => {
        if (document.querySelector(selector)) {
          resolve(document.querySelector(selector));
          observer.disconnect();
        }
      });

      observer.observe(document.body, {
        childList: true,
        subtree: true,
      });
    });
  };

  const clearTable = async () => {
    if (!$("#table_1").length) return;
    $("#table_1").DataTable().search("").draw();
    await buildSelectBox();
  };

  let observer;

  function observe() {
    observer = new MutationObserver(async (mutations) => {
      // Fix multiple links inside the table
      // table tbody tr td.column-link-to-organisationresearch-unit
      fixTableEntries();
    });

    observer.observe(document.body, {
      childList: true,
      subtree: true,
      attributes: false,
      characterData: false,
    });
  }

  if (!$("#table_1").length) {
    if (observer) observer.disconnect();
    return;
  }

  let table = $("#table_1").DataTable();
  await buildSelectBox();
  observe();
  fixTableEntries();

  // Clear button
  waitForElement("button.wdt-clear-filters-button")
    .then((el) => {
      el.addEventListener("click", clearTable, true);
    })
    .catch((error) => {
      console.log("Element not found");
    });

  $("div[data-wpdatatable_id='30']").addClass("text-big");

  // Fixes the max-height of the dropdown lists
  const countryBased = document.querySelector(
    ".column-country .dropdown-menu ul.dropdown-menu.inner"
  );

  const cssObserver = new MutationObserver(function(mutations) {
    mutations.forEach(function(mutationRecord) {
      const style = {
        maxHeight: "450px",
      };
      $(countryBased).css(style);
    });
  });

  const config = {
    attributes: true,
    attributeFilter: ["style"],
  };

  cssObserver.observe(countryBased, config);
});
