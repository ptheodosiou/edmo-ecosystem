jQuery(document).ready(async function ($) {
  const waitFor = (ms = 500) => new Promise((r) => setTimeout(r, ms));

  const waitForElement = (selector) => {
    return new Promise((resolve) => {
      if (document.querySelector(selector)) {
        return resolve(document.querySelector(selector));
      }

      const observer = new MutationObserver((mutations) => {
        if (document.querySelector(selector)) {
          resolve(document.querySelector(selector));
          observer.disconnect();
        }
      });

      observer.observe(document.body, {
        childList: true,
        subtree: true,
      });
    });
  };

  try {
    const navSelector =
      ".site-header .header-main .site-navigation .menu-item-widgets .menu-item-socials .socials";
    const navSelector2 =
      ".site-header .menu-item-widget .menu-item-socials .socials";
    const fixSocials = (element) => {
      const children = [...$(element).children()];
      const contactIconSrc =
        "https://edmo-staging.eu/wp-content/uploads/2022/03/icons8-circled-envelope-48.png";
      const twitterIconSrc =
        "https://edmo-staging.eu/wp-content/uploads/2022/03/icons8-twitter-circled-48.png";

      children.forEach((childEl) => {
        const titleAttr = $(childEl).attr("title");
        let isContact = titleAttr.toLowerCase() === "contact";

        const imgEl = `<img title="${titleAttr}" class="social-item-img" src="${
          isContact ? contactIconSrc : twitterIconSrc
        }"/>`;

        $(childEl).html(imgEl);
      });
    };
    waitForElement(navSelector)
      .then((element) => {
        fixSocials(element);
      })
      .catch((error) => {
        console.log("Something went wrong with navSelector");
        console.log(error.toString());
        console.log(error);
      });

    waitForElement(navSelector2)
      .then((element) => {
        fixSocials(element);
      })
      .catch((error) => {
        console.log("Something went wrong with navSelector");
        console.log(error.toString());
        console.log(error);
      });

    //     const selector = "#footer-nav .col-md-3.col-md-push-9";
    //     $(selector).remove();

    //     const aSelector = "#footer-nav .footer-site-info";
    //     $(aSelector).parent().removeClass("col-md-3 col-md-pull-9");
    //     $(aSelector).parent().addClass("col-md-6 col-md-pull-6");

    //     const bSelector = "#footer-navigation";
    //     $(bSelector).parent().addClass("col-md-push-6");

    // Remove unwanted categories
    const eventsElSelector =
      "a[href='https://edmo-staging.eu/category/events_2/']";
    $(eventsElSelector).remove();
    const uncategorisedElSelector =
      "a[href='https://edmo-staging.eu/category/uncategorized/']";
    $(uncategorisedElSelector).remove();
  } catch (error) {
    console.log("Something went wrong");
    console.log(error.toString());
    console.log(error);
  }
});
