jQuery(document).ready(async function ($) {
  const waitFor = (ms = 500) => new Promise((r) => setTimeout(r, ms));
  const shouldRun =
    window.location.pathname === "/list-of-institutions-connected-to-edmo/";

  if (!shouldRun) return;

  function buildListItem(value, index) {
    return `<li data-original-index="${index}"><a tabindex="${index}" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">${value}</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>`;
  }

  async function buildSelectBox() {
    await waitFor();

    // Type of institution
    let toiItems = $(".column-type-of-institution select option");
    let _toiItems = [
      ...toiItems.map(function () {
        return $(this).text();
      }),
    ]
      .join(";")
      .split(";")
      .map((val) => String(val).trim())
      .filter((item, index, array) => array.indexOf(item) === index)
      .sort();

    $(
      ".column-type-of-institution select, .column-type-of-institution ul"
    ).empty();

    _toiItems.forEach((c, i) => {
      $(".column-type-of-institution select").append(
        `<option calue="${escape(c)}">${c}</option>`
      );
      $(".column-type-of-institution ul").append(buildListItem(c, i));
    });
    await waitFor();

    $(".column-type-of-institution ul li[data-original-index]").on(
      "click",
      function () {
        $(this).toggleClass("selected");
        if ($(this).hasClass("selected")) {
          $(this).find("a").attr("aria-selected", true);
          return;
        } else {
          $(this).find("a").attr("aria-selected", false);
          return;
        }
      }
    );

    // Country in which it is based
    let cbItems = $(".column-country-in-which-it-is-based select option");
    let _cbItems = [
      ...cbItems.map(function () {
        return $(this).text();
      }),
    ]
      .join(";")
      .split(";")
      .map((val) => String(val).trim())
      .filter((item, index, array) => array.indexOf(item) === index)
      .sort();

    $(
      ".column-country-in-which-it-is-based select, .column-country-in-which-it-is-based ul"
    ).empty();

    _cbItems.forEach((c, i) => {
      $(".column-country-in-which-it-is-based select").append(
        `<option calue="${escape(c)}">${c}</option>`
      );
      $(".column-country-in-which-it-is-based ul").append(buildListItem(c, i));
    });
    await waitFor();

    $(".column-country-in-which-it-is-based ul li[data-original-index]").on(
      "click",
      function () {
        $(this).toggleClass("selected");
        if ($(this).hasClass("selected")) {
          $(this).find("a").attr("aria-selected", true);
          return;
        } else {
          $(this).find("a").attr("aria-selected", false);
          return;
        }
      }
    );

    // Hub / SSOMP name (Name of the project):
    let nopItems = $(
      "#table_1_4_filter_sections > #table_1_4_filter select option"
    );
    let _nopItems = [
      ...nopItems.map(function () {
        return $(this).text();
      }),
    ]
      .join(";")
      .split(";")
      .map((val) => String(val).trim())
      .filter((item, index, array) => array.indexOf(item) === index)
      .sort();

    $(
      "#table_1_4_filter_sections > #table_1_4_filter select, #table_1_4_filter_sections > #table_1_4_filter ul"
    ).empty();

    _nopItems.forEach((c, i) => {
      $("#table_1_4_filter_sections > #table_1_4_filter select").append(
        `<option calue="${escape(c)}">${c}</option>`
      );
      $("#table_1_4_filter_sections > #table_1_4_filter ul").append(
        buildListItem(c, i)
      );
    });
    await waitFor();

    $(
      "#table_1_4_filter_sections > #table_1_4_filter ul li[data-original-index]"
    ).on("click", function () {
      $(this).toggleClass("selected");
      if ($(this).hasClass("selected")) {
        $(this).find("a").attr("aria-selected", true);
        return;
      } else {
        $(this).find("a").attr("aria-selected", false);
        return;
      }
    });
  }

  const waitForElement = (selector) => {
    return new Promise((resolve) => {
      if (document.querySelector(selector)) {
        return resolve(document.querySelector(selector));
      }

      const observer = new MutationObserver((mutations) => {
        if (document.querySelector(selector)) {
          resolve(document.querySelector(selector));
          observer.disconnect();
        }
      });

      observer.observe(document.body, {
        childList: true,
        subtree: true,
      });
    });
  };

  const clearTable = async () => {
    if (!$("#table_1").length) return;
    $("#table_1").DataTable().search("").draw();
    await buildSelectBox();
  };

  let observer;

  function observe() {
    observer = new MutationObserver(async (mutations) => {
      // Table fix - Adds the number of total 'th's as the colspan for the row-details elements.
      let columns = $("#table_1 thead th").length;
      let rowDetails = $("tr.row-detail");
      if (rowDetails.length) {
        rowDetails.each(function (el, index) {
          $(this).find("td").attr("colspan", columns);
        });
      }
    });

    observer.observe(document.body, {
      childList: true,
      subtree: true,
      attributes: false,
      characterData: false,
    });
  }

  if (!$("#table_1").length) {
    if (observer) observer.disconnect();
    return;
  }

  let table = $("#table_1").DataTable();
  await buildSelectBox();
  observe();

  // Clear button
  waitForElement("button.wdt-clear-filters-button")
    .then((el) => {
      el.addEventListener("click", clearTable, true);
    })
    .catch((error) => {
      console.log("Element not found");
    });

  $("div[data-wpdatatable_id='22']").addClass("text-big");

  // Fixes the max-height of the dropdown lists
  const typeOfInstitution = document.querySelector(
    ".column-type-of-institution .dropdown-menu ul.dropdown-menu.inner"
  );
  const countryBased = document.querySelector(
    ".column-country-in-which-it-is-based .dropdown-menu ul.dropdown-menu.inner"
  );
  const nameOfProjects = document.querySelector(
    "#table_1_4_filter_sections > #table_1_4_filter .dropdown-menu ul.dropdown-menu.inner"
  );
  const cssObserver = new MutationObserver(function (mutations) {
    mutations.forEach(function (mutationRecord) {
      const style = {
        maxHeight: "450px",
      };
      $(typeOfInstitution).css(style);
      $(countryBased).css(style);
      $(nameOfProjects).css(style);
    });
  });

  const config = {
    attributes: true,
    attributeFilter: ["style"],
  };

  cssObserver.observe(typeOfInstitution, config);
  cssObserver.observe(countryBased, config);
  cssObserver.observe(nameOfProjects, config);
});
