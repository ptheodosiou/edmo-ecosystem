jQuery(document).ready(async function ($) {
  const waitFor = (ms = 500) => new Promise((r) => setTimeout(r, ms));
  const shouldRun = window.location.pathname === "/scientific-publications/";

  if (!shouldRun) return;

  function buildListItem(value, index) {
    return `<li data-original-index="${index}"><a tabindex="${index}" data-tokens="null" role="option" aria-disabled="false" aria-selected="false"><span class="text">${value}</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li>`;
  }

  async function buildSelectBox() {
    await waitFor();

    // Reginal Interest
    let riItems = $(".column-regional-interest select option");
    let _riItems = [
      ...riItems.map(function () {
        return $(this).text();
      }),
    ]
      .join(";")
      .split(";")
      .map((val) => String(val).trim())
      .filter((item, index, array) => array.indexOf(item) === index)
      .sort();

    $(".column-regional-interest select, .column-regional-interest ul").empty();

    _riItems.forEach((c, i) => {
      $(".column-regional-interest select").append(
        `<option calue="${escape(c)}">${c}</option>`
      );
      $(".column-regional-interest ul").append(buildListItem(c, i));
    });
    await waitFor();

    $(".column-regional-interest ul li[data-original-index]").on(
      "click",
      function () {
        $(this).toggleClass("selected");
        if ($(this).hasClass("selected")) {
          $(this).find("a").attr("aria-selected", true);
          return;
        } else {
          $(this).find("a").attr("aria-selected", false);
          return;
        }
      }
    );

    // Field of research
    let farItems = $(".column-field-of-research select option");
    let _farItems = [
      ...farItems.map(function () {
        return $(this).text();
      }),
    ]
      .join(";")
      .split(";")
      .map((val) => String(val).trim())
      .filter((item, index, array) => array.indexOf(item) === index)
      .sort();

    $(".column-field-of-research select, .column-field-of-research ul").empty();

    _farItems.forEach((c, i) => {
      $(".column-field-of-research select").append(
        `<option calue="${escape(c)}">${c}</option>`
      );
      $(".column-field-of-research ul").append(buildListItem(c, i));
    });
    await waitFor();

    $(".column-field-of-research ul li[data-original-index]").on(
      "click",
      function () {
        $(this).toggleClass("selected");
        if ($(this).hasClass("selected")) {
          $(this).find("a").attr("aria-selected", true);
          return;
        } else {
          $(this).find("a").attr("aria-selected", false);
          return;
        }
      }
    );

    // Regional Affiliation of Researchers
    let rarItems = $(
      ".column-regional-affiliation-of-researchers select option"
    );
    let _rarItems = [
      ...rarItems.map(function () {
        return $(this).text();
      }),
    ]
      .join(";")
      .split(";")
      .map((val) => String(val).trim())
      .filter((item, index, array) => array.indexOf(item) === index)
      .sort();

    $(
      ".column-regional-affiliation-of-researchers select, .column-regional-affiliation-of-researchers ul"
    ).empty();

    _rarItems.forEach((c, i) => {
      $(".column-regional-affiliation-of-researchers select").append(
        `<option calue="${escape(c)}">${c}</option>`
      );
      $(".column-regional-affiliation-of-researchers ul").append(
        buildListItem(c, i)
      );
    });
    await waitFor();

    $(
      ".column-regional-affiliation-of-researchers ul li[data-original-index]"
    ).on("click", function () {
      $(this).toggleClass("selected");
      if ($(this).hasClass("selected")) {
        $(this).find("a").attr("aria-selected", true);
        return;
      } else {
        $(this).find("a").attr("aria-selected", false);
        return;
      }
    });
  }

  const waitForElement = (selector) => {
    return new Promise((resolve) => {
      if (document.querySelector(selector)) {
        return resolve(document.querySelector(selector));
      }

      const observer = new MutationObserver((mutations) => {
        if (document.querySelector(selector)) {
          resolve(document.querySelector(selector));
          observer.disconnect();
        }
      });

      observer.observe(document.body, {
        childList: true,
        subtree: true,
      });
    });
  };

  const clearTable = async () => {
    if (!$("#table_1").length) return;
    $("#table_1").DataTable().search("").draw();
    await buildSelectBox();
  };

  let observer;

  function observe() {
    observer = new MutationObserver(async (mutations) => {
      // Table fix - Adds the number of total 'th's as the colspan for the row-details elements.
      let columns = $("#table_1 thead th").length;
      let rowDetails = $("tr.row-detail");
      if (rowDetails.length) {
        rowDetails.each(function (el, index) {
          $(this).find("td").attr("colspan", columns);
        });
      }
    });

    observer.observe(document.body, {
      childList: true,
      subtree: true,
      attributes: false,
      characterData: false,
    });
  }

  if (!$("#table_1").length) {
    if (observer) observer.disconnect();
    return;
  }

  let table = $("#table_1").DataTable();
  await buildSelectBox();
  observe();

  $("#table_1_range_from_3, #table_1_range_to_3").change(function () {
    console.log("Table will be re-draw cause of number change");
    table.draw();
  });

  $("div[data-wpdatatable_id='20']").addClass("text-big");

  // Clear button
  waitForElement("button.wdt-clear-filters-button")
    .then((el) => {
      el.addEventListener("click", clearTable, true);
    })
    .catch((error) => {
      console.log("Element not found");
    });

  // Fixes the max-height of the dropdown lists
  const regionalInterest = document.querySelector(
    ".column-regional-interest .dropdown-menu ul.dropdown-menu.inner"
  );
  const fieldOfResearch = document.querySelector(
    ".column-field-of-research .dropdown-menu ul.dropdown-menu.inner"
  );
  const regionalAffiliation = document.querySelector(
    ".column-regional-affiliation-of-researchers .dropdown-menu ul.dropdown-menu.inner"
  );
  const cssObserver = new MutationObserver(function (mutations) {
    mutations.forEach(function (mutationRecord) {
      const style = {
        maxHeight: "450px",
      };
      $(regionalInterest).css(style);
      $(fieldOfResearch).css(style);
      $(regionalAffiliation).css(style);
    });
  });

  const config = {
    attributes: true,
    attributeFilter: ["style"],
  };

  cssObserver.observe(regionalInterest, config);
  cssObserver.observe(fieldOfResearch, config);
  cssObserver.observe(regionalAffiliation, config);
});
